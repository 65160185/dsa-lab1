import java.util.Arrays;
import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        int[] numbers = {5, 8, 3, 2, 7};
        String[] names = {"Alice", "Bob", "Charlie", "David"};
        Double[] values = new Double[4];

        for(int i = 0; i < numbers.length; i++){
            System.out.print(numbers[i]+" ");
        }
        System.out.println();

        for(String i : names){
            System.out.print(i+" ");
        }
        System.out.println();

        for(int i = 0;i < values.length;i++){
            Scanner sc = new Scanner(System.in);
            values[i] = sc.nextDouble();
        }

        int sum = Arrays.stream(numbers).sum();
        System.out.println(sum);


        int max = Arrays.stream(numbers).max().getAsInt();
        System.out.println(max);

        String[] reversedNames = new String[names.length];

        for (int i = 0; i < names.length; i++) {
            reversedNames[i] = names[names.length - 1 - i];
        }

        for (String name : reversedNames) {
            System.out.print(name+" ");
        }
    }
}
