public class lab012 {
    public static void duplicateZeros(int[] arr) {
        int i = 0;
        while (i < arr.length) {
            if (arr[i] == 0) {
                for (int j = arr.length - 1; j > i; j--) {
                    arr[j] = arr[j - 1];
                }
                i += 2;
            } else {
                i++;
            }
        }
    }

    public static void main(String[] args) {
        int[] arr1 = {1, 0, 2, 3, 0, 4, 5, 0};
        int[] arr2 = {1,2,3};
        duplicateZeros(arr1);
        for (int num : arr1) {
            System.out.print(num + " ");
        }
        System.out.println();
        duplicateZeros(arr2);
        for (int num : arr2) {
            System.out.print(num + " ");
        }
    }
}
